﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class UIManager : MonoBehaviour {

	public Blur blurEffect;
	public GameObject UI;

	private bool UIEnabled = false;
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp (KeyCode.Escape))
		{
			if (UIEnabled) 
			{
				UIEnabled = false;
				blurEffect.enabled = false;
				UI.gameObject.SetActive (false);
			} 
			else 
			{
				UIEnabled = true;
				blurEffect.enabled = true;
				UI.gameObject.SetActive (true);
			}
		}
	
	}
}
