﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityStandardAssets.ImageEffects;

public class AtmospthereManager : MonoBehaviour {

	public Bloom bloomEffect;
	public NoiseAndGrain noiseEffect;
	public ColorCorrectionCurves colorCorrectionEffect;

	private bool happy;

	// Use this for initialization
	void Start () {

		happy = true;
		bloomEffect.bloomThreshold = 0.5f;

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyUp (KeyCode.Space)) 
		{
			happy = !happy;

			if (happy) 
			{
				DOTween.To (()=>bloomEffect.bloomThreshold ,
					(x)=>bloomEffect.bloomThreshold = x ,
					0.5f,
					0.5f);

				DOTween.To (()=>noiseEffect.intensityMultiplier ,
					(x)=>noiseEffect.intensityMultiplier = x ,
					0f,
					0.5f);

				DOTween.To (()=>colorCorrectionEffect.saturation ,
					(x)=>colorCorrectionEffect.saturation = x ,
					1f,
					0.5f);
			} 
			else 
			{
				DOTween.To (()=>bloomEffect.bloomThreshold ,
					(x)=>bloomEffect.bloomThreshold = x ,
					1f,
					0.5f);

				DOTween.To (()=>noiseEffect.intensityMultiplier ,
					(x)=>noiseEffect.intensityMultiplier = x ,
					0.74f,
					0.5f);

				DOTween.To (()=>colorCorrectionEffect.saturation ,
					(x)=>colorCorrectionEffect.saturation = x ,
					0.2f,
					0.5f);
			}
		}
	}
}
